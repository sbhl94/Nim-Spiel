Zu dem Team gehören Markus Schütz und Sebastian Biehl. 


Aufgabenbereich Markus Schütz:

Markus Schütz ist für die Erarbeitung eines Feinkonzepts und die Umsetzung der ersten Basisfunktionen für den Spielaufbau zuständig. Des Weiteren programmiert er die Auswahloptionen, die man zu Beginn des Spiels besitzt. An der Programmierung des Spielablaufs ist er ebenfalls beteiligt. Des Weiteren testet er das Programm in Black-Box- und White-Box-Tests auf Bugs und Logik-Fehler und behebt diese.


Aufgabenbereich Sebastian Biehl:

Sebastian Biehl ist hauptsächlich für die Programmierung des Spielverlaufs zuständig. Er befasst sich mit der Lösung von Problemen, die während der Programmierung auftauchen, und welche Möglichkeiten es  zur technischen Umsetzung der im Feinkonzept erarbeiteten Features gibt. An der Erarbeitung des Konzepts ist er ebenfalls beteiligt, indem er Ideen einbringt und das Konzept kritisch überprüft. Auch er ist selbstverständlich am Test des Spiels und eventueller Fehlerbehebung beteiligt.
