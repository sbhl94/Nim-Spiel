//+-/// Nim-Spiel.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include "string.h."
#include <time.h>
#include <ctype.h>

void streichhoelzer(int rest)
{
	//Streichh�lzer zeichnen
	for (int i = 0; i < rest;i++)
	{
		printf("|");
	}
	printf("\n");
}

void leer(int *reihe, bool *leer)
{
	//Pr�fung, ob Reihe leer
	if (*reihe == 0)
	{
		printf("Es befinden sich keine Streichh�lzer mehr in dieser Reihe. Welche Reihe wollen Sie nehmen?");
		*leer = true;
	}
	else {
		*leer = false;
	}
}

void entfernen(int *reihe, int *nehmen, int *reihe1, int *reihe2, int *reihe3, int *reihe4)
{
	//Streichh�lzer entfernen
	switch (*reihe) {
	case 1: *reihe1 -= *nehmen;break;
	case 2: *reihe2 -= *nehmen;break;
	case 3: *reihe3 -= *nehmen;break;
	case 4: *reihe4 -= *nehmen;break;
	}
}

void main()
{
	bool reiheLeer = false;
	bool reihe1Leer = false;
	bool reihe2Leer = false;
	bool reihe3Leer = false;
	bool reihe4Leer = false;
	bool wiederholung = false;
	bool computergegner = true;

	int anfangen = 0;
	int anzahlLeer = 0;
	int reiheSpieler = 0;
	int shSpieler = 0;
	int max;
	int kiReihe;
	int kiNehmen;
	int reihe1 = 1;
	int reihe2 = 3;
	int reihe3 = 5;
	int reihe4 = 7;

	char variante[2] = "k";
	char *spieler = "Spieler 1";
	char ch;

	srand(time(NULL));

	printf("Willkommen zum Nim-Spiel.\nSpielregeln: Jeder Spieler nimmt abwechselnd eine beliebige Anzahl an Streichhoelzern aus einer Reihe. Wer das letzte Streichholz nimmt, gewinnt bzw. verliert (je nach Variante).\nMoechten Sie gegen einen menschlichen oder einen KI-Gegner antreten? (m/k) ");
	do {
		fgets(variante, 2, stdin);
		if (variante[0] != 'm' && variante[0] != 'k') printf("Ungueltige Eingabe. Wiederholen Sie die Eingabe.\n", variante[0]);
	} while (variante[0] != 'm' && variante[0] != 'k');

	if (variante[0] == 'm') computergegner = false;

	printf("Waehlen Sie bitte zu Beginn die  Spiel - Variante.\nIm klassischen Modus gewinnt der Spieler, welcher das letzte Streichholz aufnimmt.\nBei der Misere - Variante verliert der Spieler, welcher das letzte Streichholz aufnehmen muss.\nKlassische oder Misere - Variante ? (k / m) ");

	//Spielvariante festlegen
	do {
		while ((ch = getchar()) != '\n' && ch != EOF);
		fgets(variante, 2, stdin);
		if (variante[0] != 'm' && variante[0] != 'k') printf("Ungueltige Eingabe. Wiederholen Sie die Eingabe.\n", variante[0]);
	} while (variante[0] != 'm' && variante[0] != 'k');

	//Beginner per Zufall festlegen
	anfangen = rand() % 2;
	if (anfangen == 0)
	{
		if (computergegner == true) printf("Der Computer beginnt.\n");
		else printf("Spieler 1 beginnt\n");
	}
	else
	{
		if (computergegner == true) printf("Sie beginnen.\n");
		else
		{
			printf("Spieler 2 beginnt\n");
			spieler = "Spieler 2";
		}
	}

	//Hauptalgorithmus
	while (reihe1 > 0 || reihe2 > 0 || reihe3 > 0 || reihe4 > 0)
	{
		//Pr�fung, ob eine Reihe leer ist
		if (reihe1Leer == false && reihe1 <= 0) {
			reihe1Leer = true;
			reihe1 = 0;
			anzahlLeer += 1;
		}
		if (reihe2Leer == false && reihe2 <= 0) {
			reihe2Leer = true;
			reihe2 = 0;
			anzahlLeer += 1;
		}
		if (reihe3Leer == false && reihe3 <= 0) {
			reihe3Leer = true;
			reihe3 = 0;
			anzahlLeer += 1;
		}
		if (reihe4Leer == false && reihe4 <= 0) {
			reihe4Leer = true;
			reihe4 = 0;
			anzahlLeer += 1;
		}

		//Streichh�lzer werden neu gezeichnet, falls kein wiederholter Durchlauf stattfindet
		if (wiederholung == false) {
			streichhoelzer(reihe1);
			streichhoelzer(reihe2);
			streichhoelzer(reihe3);
			streichhoelzer(reihe4);
		}

		//Spielerzug
		if ((anfangen == 1 && computergegner == true) || computergegner == false) {
			//Reihenauswahl
			if (computergegner == false)
			{
				printf("%s, welche Reihe wollen Sie nehmen? ", spieler);
				if (anfangen == 1) spieler = "Spieler 1";
				else spieler = "Spieler 2";
			}
			else printf("Welche Reihe wollen Sie nehmen? ");

			do {
				while ((ch = getchar()) != '\n' && ch != EOF);
				scanf_s("%d", &reiheSpieler);

				if (reiheSpieler <1 || reiheSpieler >4) printf("Ungueltige Eingabe. Wiederholen Sie die Eingabe.\n");

				//Pr�fung auf leere Reihen
				switch (reiheSpieler) {
				case 1: leer(&reihe1, &reiheLeer);break;
				case 2: leer(&reihe2, &reiheLeer);break;
				case 3: leer(&reihe3, &reiheLeer);break;
				case 4: leer(&reihe4, &reiheLeer);break;
				}
			} while (reiheLeer == true || reiheSpieler < 1 || reiheSpieler > 4);

			//Anzahl Streichh�lzer ausw�hlen
			printf("Wie viele Streichh�lzer wollen Sie nehmen? ");
			do {
				while ((ch = getchar()) != '\n' && ch != EOF);
				scanf_s("%d", &shSpieler);
				if (scanf_s("d", &shSpieler) == 1 || shSpieler < 1) printf("Ungueltige Eingabe. Wiederholen Sie die Eingabe.\n");
			} while (scanf_s("d", &shSpieler) == 1 || shSpieler < 1);

			//Streichh�lzer aus Reihe entfernen
			entfernen(&reiheSpieler, &shSpieler, &reihe1, &reihe2, &reihe3, &reihe4);

			//Wer ist dran?
			if(computergegner == true || anfangen == 1) anfangen = 0;
			else anfangen = 1;
		}
		//KI-Zug
		else if (anfangen == 0 && computergegner == true) {

				//Reihenauswahl per Zufallsgenerator
				kiReihe = rand() % 4 + 1;

				//Maximale Streichholzzahl festlegen
				switch (kiReihe) {
				case 1: max = reihe1;break;
				case 2: max = reihe2;break;
				case 3: max = reihe3;break;
				case 4: max = reihe4;break;
				}

				//Wenn noch Streichh�lzer in der ausgew�hlten Reihe sind, f�hrt der Computer seinen Zug durch
				if (max > 0) {
					//Anzahl Streichh�lzer per Zufallsgenerator bestimmen
					kiNehmen = rand() % max + 1;

					//KI-Strategie einbauen
					if (anzahlLeer == 3 && variante[0] == 'k') kiNehmen = max;
					else if (anzahlLeer == 3 && variante[0]== 'm'  && max>1) kiNehmen = max - 1;
					else if (anzahlLeer == 2)
					{
						kiNehmen = max - (reihe4 + reihe3 + reihe2 + reihe1 - max);
						if ((kiNehmen == 0 || ((max - kiNehmen) == 1) && variante[0] == 'm')) kiNehmen++;
					}

					if (kiNehmen > 0)
					{
						entfernen(&kiReihe, &kiNehmen, &reihe1, &reihe2, &reihe3, &reihe4); //Streichh�lzer entfernen
						printf("Der Computer ist am Zug.");
						printf("Der Computer hat in Reihe %d %d Streichh�lzer entfernt.\n", kiReihe, kiNehmen);
						wiederholung = false; //Der n�chste Zug ist keine Wiederholung
						anfangen = 1; //Nach dem ausgef�hrten KI-Zug ist der Spieler wieder dran
					}
					else wiederholung = true;
				}
				else wiederholung = true; //Wenn in der ausgew�hlten Reihe keine Streichh�lzer mehr vorhanden sind, wird der Zug wiederholt
			}
		}

	//Spielende

	//Abschlie�ende Zeichnung der Streichh�lzer, um zu beweisen, dass keine Streichh�lzer mehr vorhanden sind
	streichhoelzer(reihe1);
	streichhoelzer(reihe2);
	streichhoelzer(reihe3);
	streichhoelzer(reihe4);

	//Siegerbestimmung je nach Variantenauswahl
	if (variante[0] == 'm')
	{
		if (computergegner == true)
		{
			if (anfangen == 1)
			{
				printf("Glueckwunsch! Sie haben gewonnen.");
			}
			else
			{
				printf("Der Computer gewinnt.");
			}
		}
		else
		{
			if (spieler = "Spieler 1") printf("Spieler 2 gewinnt!");
			else printf("Spieler 1 gewinnt!");
		}
	}
	else
	{
		if (computergegner == true)
		{
			if (anfangen == 0)
			{
				printf("Glueckwunsch! Sie haben gewonnen.");
			}
			else
			{
				printf("Der Computer gewinnt.");
			}
		}
		else
		{
			if (spieler = "Spieler 1") printf("Spieler 1 gewinnt!");
			else printf("Spieler 2 gewinnt!");
		}
	}
}

