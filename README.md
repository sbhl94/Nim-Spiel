Dieses Projekt befasst sich mit dem sogenannten »Nim-Spiel«. Das Spiel wird von zwei Spielern gespielt. Ausgangspunkt des Spiels sind vier Reihen von Gegenständen, welche in unserer Version als Streichhölzer dargestellt werden. In jeder Reihe gibt es eine unterschiedliche Anzahl von Streichhölzern. In unserer Version werden die Streichhölzer voraussichtlich wie folgt verteilt sein:

I

III

IIIII

IIIIIII

Spielablauf: Jeder Spieler kann abwechselnd eine beliebige Anzahl an Streichhölzern aus einer Reihe wegnehmen. Gewinner ist derjenige, welcher das letzte Streichholz erwischt. In der alternativen Variante (Misère-Variante) verliert der Spieler, welcher das letzte Hölzchen nehmen muss. 

Programmiertechnische Umsetzung: Der Spieler kann in seinem Zug die Reihe auswählen, aus der er Streichhölzer entfernen will, und dann die Menge der zu entfernenden Streichhölzer angeben.
Eine der technischen Herausforderungen wird es sein, wie das Programm erkennt, dass alle Streichhölzer aus dem Spiel genommen wurden. Hierfür könnte eine Zählervariable integriert werden, welche sich jedes Mal erhöht, wenn ein Streichholz ausgewählt wird. Zu Beginn jedes Zugs wird dann überprüft, ob alle Streichhölzer aus dem Spiel entfernt wurden. Hier muss man aber in der Programmierung aufpassen, dass diese Variable auch immer korrekt gezählt wird, denn schon ein kleiner Fehler kann dazu führen, dass die Variable falsch gezählt wird und dementsprechend das Spielende verfälscht wird.

Eine weitere programmiertechnische Herausforderung wird die Implementierung einer KI sein. Diese könnte mit einem Zufallsgenerator arbeiten. Zusätzlich könnten spezielle Verhaltensweisen implementiert werden, die den Computer sinnvoll handeln lassen. Diese könnten zum Beispiel von der Menge der noch verfügbaren Reihen oder der Streichholzanzahl abhängig sein. Man muss aber darauf achten, die KI nicht so perfekt zu programmieren, dass der Spieler keine Chance hat, das Spiel zu gewinnen. Deshalb sollte hier eine sinnvolle Balance gefunden werden. Die KI in unserem Spiel soll keine perfekte Strategie haben, aber in gewissen Situationen logisch handeln können. So wie es ein Spieler tun würde, der das Spiel spielt, ohne eine Master-Strategie parat zu haben.
