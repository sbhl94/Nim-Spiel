20.12.15 -> Readme, Contributing & Changelog erstellt

02.01.15 -> Basisfunktionen eingerichtet

03.01.15 -> KI eingerichtet, zweiten Spiel-Modus hinzugefügt (Misere-Variante)

04.01.15 -> Tests und Bugfixing

06.01.15 -> Zweispieler-Modus eingefügt

06.01.15 -> Tests und Bugfixing

07.01.15 -> KI verbessert

09.01.15 -> Abschließende Tests und "Release"